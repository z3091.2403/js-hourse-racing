let horsesName = ["Арабский скакун", "Старая кляча", "Лошадка", "Лихой жеребец", "Ослик"];
let horses = [];
let account = 0;

function Horse(name) {
    this.name = name;
    this.isWinner = false;
    this.wager = 0;

    this.run = function() {
        const timeout = Math.round(Math.random() * (3000 - 500) + 500);         
        return new Promise((resolve) => {
            setTimeout(() => {
                console.log(`Лошадка '${this.name}' прибежала через ${timeout} секунд!`);
                resolve(this);
            }, timeout);
        })
    }
}

for (const horse of horsesName) {
    horses.push(new Horse(horse));
}

function showHorses() {
    console.log(horsesName);
}

function showAccount() {
    console.log(account);
}

function setWager(horseName, wager) {
    const horseForWager = horses.find(horse => horse.name === horseName);
    const wagerNumber = +wager;

    if (isNaN(wagerNumber) || wagerNumber < 0) {
        console.log(`Такая ставка не подходит.`);
        return
    }
    else if (horseForWager == undefined ) console.log(`Лошадка с таким именем в забеге не участвует.`);
    else if (account <= 0) console.log(`Грустно, когда в кармане пусто.`);
    else if (wagerNumber > account) console.log('Не хватает средств на счете для такой ставки.');
    else if (horseForWager.wager > 0) console.log(`Ты уже поставил на эту лошадку.`);
    else {
        horseForWager.wager = wagerNumber;
        account -= wagerNumber;
    }
}

function startRacing() {
    let horsesWithWagers = horses.filter(horse => horse.wager > 0).length;
    horsesWithWagers === 0? console.log(`Кто не рискует, тот не пьет шампанское...`): console.log(`Начинаем забег!!!!`);

    let promisesArray = horses.map(horse => horse.run());

    Promise.race(promisesArray)
    .then((winner) => { 
        winner.isWinner = true;
    })

    Promise.all(promisesArray)
    .then(() => {
        let winnerHorse = horses.find(horse => horse.isWinner);
        if (winnerHorse.wager > 0) {
            account += winnerHorse.wager * 2;
            console.log(`Победитель забега ${winnerHorse.name}!`);
            console.log(`Это победа! Ты выиграл ${winnerHorse.wager*2}! У тебя на счету теперь ${account}!`);
        }
        else {
            console.log(`Победитель забега '${winnerHorse.name}'!`);
        }
        cleanWagers();
        winnerHorse.isWinner = false;
    })
}

function newGame() {
    horses.splice();
    cleanWagers();
    account = prompt(`Какой суммой готов рисковать?`);
    console.log(`У тебя на счету ${account}!
    Чтобы играть, используй команды:
    showHorses() - увидеть список лошадей
    showAccount() - узнать остаток на своем счете
    setWager('Имя', 'Ставка') - Напиши имя лошадки и сумму ставки
    startRacing() - начать забег
    newGame() - новая игра`);
}

function cleanWagers() {
    for (const horse of horses) horse.wager = 0;
}